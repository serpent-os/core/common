## Description

Onboarding documentation and convenience scripts for cloning/building/pulling/pushing the Serpent OS tooling.

For a general overview of the goals of Serpent OS and how to get in touch, see our [website](https://serpentos.com).

## Onboarding

Serpent OS tooling is written primarily in [Dlang](https://dlang.org/).

### Prerequisites

We use:

- [`git`](https://git-scm.com/) to manage development.
- [`meson`](https://mesonbuild.com/) (with [`dub`](https://dub.pm/) as a fallback) and [`ldc2`](https://wiki.dlang.org/LDC) to build our binaries. 
- [`dfmt`](https://github.com/dlang-community/dfmt) to format our code consistently. Consult the [`dfmt` README](https://github.com/dlang-community/dfmt#installation) for how to build it with LDC. Our scripts assume that `dfmt` is available in `$PATH`.
- the python module `codespell` for spell checking. Install it from your distribution's package manager.

For convenience, we maintain a `check-prereqs.sh` script, which will check for all necessary binaries, runtime libraries and development headers and report missing prerequisites.

#### Dlang Toolchain installation

The currently recommended way to install the Dlang toolchain is to use the official install script:

    curl -fsS https://dlang.org/install.sh | bash -s ldc

Remember to source the appropriate environment initialisation script from your preferred shell's user config file.

We tend to follow the newest upstream version of LDC quite closely.

### Repo structure

We use a flat repository structure where all Dlang `meson`-controlled subprojects are expected to be checked out concurrently.

This forces a "lockstep" development methodology, which means that whatever is currently checked out in each subproject is what any given binary will be built against.

This also implies that all subprojects will need to be kept in sync with the features that are being worked on (preferably using identical topic branch names).

The only place we use "full" git submodules is in `moss-vendor`.

#### Getting and building the serpent tooling

Here, all relevant Serpent OS subprojects will be checked out under `~/repos/serpent-os/`

```
# Initial setup
mkdir -pv ~/repos/serpent-os/
cd ~/repos/serpent-os/

curl https://gitlab.com/serpent-os/core/onboarding/-/raw/main/init.sh |bash
```

### Serpent tooling build order

To get started packaging with the current pre-alpha quality serpent tooling, the following binaries need to be built in the order listed below:

- [`moss`](https://gitlab.com/serpent-os/core/moss) (our system software management tool)
- [`moss-container`](https://gitlab.com/serpent-os/core/moss-container) (our lightweight container tool)
- [`boulder`](https://gitlab.com/serpent-os/core/boulder) (our system software build tool)

The `build-all.sh` script builds the serpent tooling in the order listed above.

## Support

Please refer to the website for instructions on how to get in touch with the Serpent OS developers.

## Contributing

Please get in touch with the Serpent OS developers before contributing MRs.

We're a friendly bunch and will likely welcome your contributions.

## License

Serpent OS is licensed under the Zlib license.
